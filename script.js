// 1. Створення нового HTML тегу на сторінці можно зробити за домомогою методу document.createElement("")
// let div = document.createElement("div")

// 2. Метод element.insertAdjacentHTML(where, html) має два параметри.
// Перший параметр вказує куди вставити по відношенню до element.
// Значення першого параметру може бути:
// - "beforebegin" - вставка html перед element
// - "afterbegin" - вставка html на початок element
// - "beforeend" - вставка html в кінець element
// - "afterend" - вставка html після element
// Другий параметр - це html рядок який буде вставлений як HTML

// 3. Для видалення елементів зі сторінки є метод element.remove()

let firstArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let secondArray = ["1", "2", "3", "sea", "user", 23];
function showList(inputArray, parent = document.body) {
  parent.insertAdjacentHTML("beforeend", `<ul> ${arrToList(inputArray)} </ul>`);
}
function arrToList(inputArray) {
  return inputArray
    .map((element) => {
      return `<li>${element}</li>`;
    })
    .join("");
}
showList(firstArray);
showList(secondArray);
